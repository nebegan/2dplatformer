﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform _bulletTrailPref;

	public float fireRate = 0;
	public float Damage = 10;
	public LayerMask whatToHit;
	
	float timeToFire = 0;
	Transform firePoint;

	// Use this for initialization
	void Awake ()
    {
		firePoint = transform.FindChild ("FirePoint");
		if (firePoint == null)
        {
			Debug.LogError ("No firePoint");
		}
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (fireRate == 0) {
			if (Input.GetButtonDown ("Fire1")) {
				Shoot();
			}
		}
		else
        {
			if (Input.GetButton ("Fire1") && Time.time > timeToFire) {
				timeToFire = Time.time + 1/fireRate;
				Shoot();
			}
		}
	}
	
	void Shoot ()
    {
		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, Vector2.right, 100, whatToHit);

	    ShootingEffect();

        if (hit.collider != null) {
			Debug.DrawLine (firePointPosition, hit.point, Color.red);
			Debug.Log ("We hit " + hit.collider.name + " and did " + Damage + " damage.");

            Enemy enemy = hit.collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.DamageEnemy();
            }
        }
	}

    private void ShootingEffect()
    {
        Instantiate(_bulletTrailPref, firePoint.position, firePoint.rotation);
    }
}
