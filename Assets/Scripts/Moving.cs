﻿using UnityEngine;
using System.Collections;

public class Moving : MonoBehaviour
{
    [SerializeField] private Transform _toMoveTheObject;
    public float movementSpeed;

	void Update ()
    {
        Movement();
	}

    private void Movement()
    {
        _toMoveTheObject.position= new Vector3(_toMoveTheObject.position.x - 0.8f*movementSpeed * Time.fixedDeltaTime, transform.position.y, transform.position.z);
    }
}
