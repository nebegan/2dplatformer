﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField]  private Transform _toMoveTheObject;
    [SerializeField] private SpriteRenderer _sprite;

    public float movementSpeed;
    private int _health;

    void Start()
    {
        GenerateParameters(Random.Range(0,3));
    }

    void Update()
    {
        Movement();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        GameManager gm = GameObject.FindObjectOfType<GameManager>();
        gm.GameOver();
    }

    public void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void Movement()
    {
        _toMoveTheObject.position = new Vector3(_toMoveTheObject.position.x - 0.8f * movementSpeed * Time.fixedDeltaTime, transform.position.y, transform.position.z);
    }

    public void DamageEnemy()
    {
        if (_health > 0)
            _health--;
        if (_health<=0)
            Destroy(gameObject);
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    private void GenerateParameters(int value)
    {
        switch (value)
        {
            case 0:
                _health = 1;
                movementSpeed = 10;
                _sprite.color = Color.red;

                break;

            case 1:
                _health = 3;
                movementSpeed = 5;
                _sprite.color = Color.yellow;

                break;
            case 2:
                _health = 5;
                movementSpeed = 2;
                _sprite.color = Color.green;

                break;
        }
    }
}
