﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Enemy _enemy;
    [SerializeField] private Transform _player;
    [SerializeField] private Moving _movingFloor;

    [SerializeField] private Text _gameOverText;
    [SerializeField] private Button _startButton;

    private Camera cam;

    void Awake()
    {
        cam = Camera.main;
    }

    public void GameOver()
    {
        StopAllCoroutines();

        _player.gameObject.SetActive(false);
        _gameOverText.gameObject.SetActive(true);
        _startButton.gameObject.SetActive(true);

        foreach (Enemy e in Object.FindObjectsOfType<Enemy>())
        {
            e.DestroyEnemy();
        }

        _startButton.GetComponentInChildren<Text>().text = "RESTART";
    }

    public void StartOrRestart()
    {
        StartCoroutine(GenerateEnemy());

        _player.gameObject.SetActive(true);

        if (_gameOverText.isActiveAndEnabled)
        {
            _gameOverText.gameObject.SetActive(false);
        }

        _startButton.gameObject.SetActive(false);
    }

    IEnumerator GenerateEnemy()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            Vector3 position = new Vector3(_player.transform.position.x+cam.orthographicSize * Screen.width / Screen.height, 0, 0);
            Instantiate(_enemy, position, transform.rotation);
        }
    }
}
